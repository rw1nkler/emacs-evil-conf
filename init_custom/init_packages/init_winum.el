;;
;; INIT_WINUM.EL
;;

(use-package winum
  :config
    (winum-mode 1)
  :general
    ("C-c 0"   'winum-select-window-0-or-10)
    ("C-c 1"   'winum-select-window-1)
    ("C-c 2"   'winum-select-window-2)
    ("C-c 3"   'winum-select-window-3)
    ("C-c 4"   'winum-select-window-4)
    ("C-c 5"   'winum-select-window-5)
    ("C-c 6"   'winum-select-window-6)
    ("C-c 7"   'winum-select-window-7)
    ("C-c 8"   'winum-select-window-8)
    ("C-c 9"   'winum-select-window-9)
)



