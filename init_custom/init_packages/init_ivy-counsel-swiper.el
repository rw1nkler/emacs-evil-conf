;;
;; INIT_IVY.EL
;;

(use-package ivy
  :init
    (setq ivy-use-virtual-buffers t)
    (setq ivy-count-format "(%d/%d) ")

  :config
    (ivy-mode 1)

  :general
    ("C-s"       'swiper)
    ("M-x"       'counsel-M-x)
    ("C-x C-f"   'counsel-find-file)
    ("<f1> f"    'counsel-describe-function)
    ("<f1> v"    'counsel-describe-variable)
    ("<f1> l"    'counsel-find-library)
    ("<f2> i"    'counsel-info-lookup-symbol)
    ("<f2> u"    'counsel-unicode-char)
    (ivy-minibuffer-map "RET"     #'ivy-alt-done)
    (minibuffer-local-map "C-r"   'counsel-minibuffer-history)
)

(use-package counsel-etags
  :ensure t
  :bind (("C-c c s" . counsel-etags-find-tag-at-point))
  :init
  (add-hook 'prog-mode-hook
        (lambda ()
          (add-hook 'after-save-hook
            'counsel-etags-virtual-update-tags 'append 'local)))
  :config
  (setq counsel-etags-update-interval 60)
  (add-to-list 'counsel-etags-ignore-directories "build")
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Documentation
;;   http://oremacs.com/swiper/
