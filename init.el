;;
;; INIT.EL
;;

;;;; Load init settings

  ;; Custom functions (Heve to be before keybindings)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

    (load "~/.emacs.d/init_custom/lib/fun_comment-eclipse")

  ;; General Settings
    (load "~/.emacs.d/init_custom/general-settings.el")

;;;; Download and configure packages

    (load "~/.emacs.d/init_custom/download-packages.el")

  ;; Keybindings
    (load "~/.emacs.d/init_custom/specific-settings.el")
